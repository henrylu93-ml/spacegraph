"""Functions to compare graph structures"""

import numpy as np
from spacegraph.config import PAST

EPS = 0.0001
DAMP = 0.5


def mse_compare(adj_base, all_adj_train, all_adj_test, verbose=False):
    """
    This performs a Mean Squared Error (MSE) comparison of train and test structures against a base structure.
    The MSE is calculated for each channel's incoming and outgoing edges. 

    adj_base: adjacency matrix representing some base structure to compare with
    all_adj_train: adjacency matrices of time windows of the train data. (num_windows, num_nodes, num_nodes)
    all_adj_test: adjacency matrices of time windows of the test data

    Returns:
        all_mse_train: MSE scores for each time window of the train set
        all_mse_test: MSE scores for each time window of the test set
    """
    all_mse_train = []  # store all mse from train set for each channel
    all_mse_test = []  # store all mse from test set for each channel

    num_channels = int(adj_base.shape[0] / (PAST + 1))
    for channel in range(0, num_channels):  # calculate score for each channel

        # Locate indices in adj matrix that correspond to this channel
        start = channel * (PAST + 1)
        out_base = adj_base[start:start + PAST + 1]  # outgoing edge weights to channel
        in_base = adj_base[:, start:start + PAST + 1]  # incoming edge weights
        num_ch, num_nodes = out_base.shape

        # calculate MSE for outgoing edges in train data
        num_train = len(all_adj_train)
        all_adj_train = np.array(all_adj_train)
        out_train = all_adj_train[:, start:start + PAST + 1, :]  # get current channel
        # broadcast needed to perform subtraction
        mse_out_train = out_train - np.broadcast_to(out_base, (num_train, num_ch, num_nodes))
        mse_out_train = np.mean(np.square(mse_out_train), axis=(1, 2))  # mean and square operation

        # calculate MSE for incoming edges in train data
        in_train = all_adj_train[:, :, start:start + PAST + 1]
        mse_in_train = in_train - np.broadcast_to(in_base, (num_train, num_nodes, num_ch))
        mse_in_train = np.mean(np.square(mse_in_train), axis=(1, 2))

        # calculate MSE for outgoing edges in test data
        num_test = len(all_adj_test)
        all_adj_test = np.array(all_adj_test)
        out_test = all_adj_test[:, start:start + PAST + 1, :]
        mse_out_test = out_test - np.broadcast_to(out_base, (num_test, num_ch, num_nodes))
        mse_out_test = np.mean(np.square(mse_out_test), axis=(1, 2))

        #calculate MSE for incoming edges in test data
        in_test = all_adj_test[:, :, start:start + PAST + 1]
        mse_in_test = in_test - np.broadcast_to(in_base, (num_test, num_nodes, num_ch))
        mse_in_test = np.mean(np.square(mse_in_test), axis=(1, 2))

        # combine incoming/outgoing scores
        mse_train = mse_in_train + mse_out_train
        mse_test = mse_in_test + mse_out_test

        # normalize mse of each channel based on train data
        # min_train = mse_train.min()
        max_train = mse_train.max() if mse_train.max() else 0.0001  # avoid division by 0 errors
        # mse_train = (mse_train-min_train)/(max_train-min_train)
        mse_train = mse_train / max_train
        # mse_test = (mse_test-min_train)/(max_train-min_train)
        mse_test = mse_test / max_train

        all_mse_train.append(mse_train)
        all_mse_test.append(mse_test)

    all_mse_test = np.array(all_mse_test)
    max_channel = np.argmax(all_mse_test)  # flattened index of max value
    max_channel = np.unravel_index(
        max_channel, all_mse_test.shape)[0]  # channel corresponding to row of max value

    # all_mse_train = np.sum(np.array(all_mse_train), axis=0)
    # all_mse_test = np.sum(np.array(all_mse_test), axis=0)
    all_mse_train = np.mean(np.array(all_mse_train), axis=0)
    all_mse_test = np.mean(np.array(all_mse_test), axis=0)

    return all_mse_train, all_mse_test, max_channel


def anomrank_compare(all_adj_train, all_adj_test, only_present=False, verbose=False):
    """
    Compare weighted adjacency matrices by each node's anomrank/page rank.
    This is adapted from https://github.com/minjiyoon/KDD19-AnomRank

    all_adj_train: adjacency matrices of time windows of the train data. (num_windows, num_nodes, num_nodes)
    all_adj_test: adjacency matrices of time windows of the test data
    only_present: set to True to only consider the nodes in the current timestep of the DBN, not the past timesteps
    """

    def _calc_anomrank(all_adj):
        """
        Calculate anomrank scores.
        """
        all_pr = []  # store all page ranks

        for adj in all_adj:
            adj = abs(adj)  # This currently only works with positive edge weights
            if adj.sum() == 0:
                b = 0
            else:
                b = adj.sum(axis=1) / adj.sum()  # starting vector

            # The original implementation normalizes each row.
            # However this can hide important differences in edge weights,
            # so we do not do that here.
            # adj_sum = np.sum(adj, axis=1)
            # adj[adj_sum == 0] = 1 / len(adj)
            # adj_sum[adj_sum == 0] = 1  # prevent divide-by-0 errors
            # adj = (adj.T / adj_sum).T  # divide each row by its sum

            step = 0  # track steps needed for convergence
            max_delta = EPS  # store the maximum change in page rank scores, so we know when to stop
            pr = np.array([1] * len(adj)) / len(adj)  # initialize page ranks
            previous_pr = np.zeros_like(pr)  # store previous pr to calculate change in scores

            # iterate until max_delta falls below epsilon
            while max_delta >= EPS:
                step += 1
                pr = DAMP * (adj.T @ pr) + (1 - DAMP) * b  # pagerank update
                max_delta = np.max(abs(pr - previous_pr))
                previous_pr = np.copy(pr)
            all_pr.append(pr)

        # Calculate first derivatives in discrete time
        # An array of zeros is inserted at the last time step.
        first_deriv = []
        for i in range(len(all_pr) - 1):
            first_deriv.append(all_pr[i] - all_pr[i + 1])
        first_deriv.append(np.zeros_like(all_pr[0]))
        first_deriv = np.array(first_deriv)

        # Calculate second derivatives in discrete time
        # An array of zeros is inserted at the first and last time step.
        second_deriv = []
        second_deriv.append(np.zeros_like(all_pr[0]))
        for i in range(1, len(all_pr) - 1):
            second_deriv.append(all_pr[i + 1] - 2 * all_pr[i] + all_pr[i - 1])
        second_deriv.append(np.zeros_like(all_pr[0]))
        second_deriv = np.array(second_deriv)

        # Anomaly scores are the L1 distance of first and second derivatives
        scores = abs(first_deriv) + abs(second_deriv)
        return scores

    if only_present:  # Only consider nodes at current timestep, not past nodes
        anomrank_train = _calc_anomrank(all_adj_train[:, ::PAST + 1, ::PAST + 1])
        anomrank_test = _calc_anomrank(all_adj_test[:, ::PAST + 1, ::PAST + 1])
    else:
        anomrank_train = _calc_anomrank(all_adj_train)
        anomrank_test = _calc_anomrank(all_adj_test)

    max_channel = np.argmax(anomrank_test)  # flattened index of max value
    max_channel = np.unravel_index(
        max_channel, anomrank_test.shape)[1]  # channel corresponding to row of max value
    max_channel = int(max_channel / (PAST + 1))

    # Normalize scores of each channel based on max of train data
    max_train = 1
    # max_train = np.max(anomrank_train, axis=0)
    # max_train[max_train == 0] = 0.0001  # prevent divide-by-zero error
    anomrank_train = np.mean(anomrank_train / max_train, axis=1)
    anomrank_test = np.mean(anomrank_test / max_train, axis=1)

    return anomrank_train, anomrank_test, max_channel
