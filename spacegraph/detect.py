"""Dynamic thresholding, taken from telemanom"""

import numpy as np
import more_itertools as mit
from spacegraph.config import PRUNE_PERCENT, STRIDE, WINDOW_LEN


def thresh_and_prune(test_err, train_err=None, verbose=False):
    """
    Score thresholding and pruning method, taken from Telemanom (Hundman et al., 2018).

    test_err: test error scores on which to run thresholding and pruning
    train_err: train error scores. if provided, will be prepended to test_err for thresholding and pruning

    Returns:
        pruned_anom: a list of lists of anomaly windows: [[start1,end1],[start2,end2]]
        epsilon: the dynamically determined threshold

    See (https://github.com/khundman/telemanom) for original. In this version, anomalies are not grouped into sequences
    until the end.
    """
    sd_lim = 12.0
    sd_threshold = sd_lim
    error_buffer = 2  # number of values surrounding an error that are brought into the sequence (promotes grouping on nearby sequences
    max_score = -10000000
    if train_err is not None:
        err = np.concatenate([train_err, test_err])
    else:
        err = test_err
    mean_err = np.mean(err)
    sd_err = np.std(err)
    epsilon = mean_err + sd_lim * sd_err

    for z in np.arange(2.5, sd_lim, 0.5):
        eps = mean_err + (sd_err * z)

        pruned_err = err[err < eps]

        i_anom = np.argwhere(err >= eps).reshape(-1,)
        buffer = np.arange(1, error_buffer)
        i_anom = np.sort(
            np.concatenate((i_anom, np.array([i + buffer for i in i_anom]).flatten(),
                            np.array([i - buffer for i in i_anom]).flatten())))
        i_anom = i_anom[(i_anom < len(err)) & (i_anom >= 0)]
        i_anom = np.sort(np.unique(i_anom))

        if len(i_anom) > 0:
            # group anomalous indices into continuous sequences
            groups = [list(group) for group in mit.consecutive_groups(i_anom)]
            E_seq = [(g[0], g[-1]) for g in groups if not g[0] == g[-1]]

            mean_perc_decrease = (mean_err - np.mean(pruned_err)) / mean_err
            sd_perc_decrease = (sd_err - np.std(pruned_err)) / sd_err
            score = (mean_perc_decrease + sd_perc_decrease) / (len(E_seq)**2 + len(i_anom))

            # sanity checks / guardrails
            if score >= max_score and len(E_seq) <= 5 and len(i_anom) < (len(err) * 0.5):
                max_score = score
                sd_threshold = z
                epsilon = mean_err + z * sd_err

    # Prune anomalies
    i_anom = np.argwhere(err >= epsilon)

    non_anom = np.copy(err)
    non_anom[i_anom] = 0
    non_anom_max = non_anom.max()

    anom = err[i_anom]
    anom_sorted = np.sort(anom)[::-1]
    anom_sorted = np.append(anom_sorted, [non_anom_max])

    i_to_remove = []
    for i in range(0, len(anom_sorted) - 1):
        if (anom_sorted[i] - anom_sorted[i + 1]) / anom_sorted[i] < PRUNE_PERCENT:
            i_to_remove.append(np.argwhere(anom == anom_sorted[i]))
        else:
            i_to_remove = []

    if len(i_to_remove) > 0:
        # anom = np.delete(anom, i_to_remove, axis=0)
        i_anom = np.delete(i_anom, i_to_remove, axis=0)

    # Group consecutive anomalies together into a single anomaly.
    groups = [list(group) for group in mit.consecutive_groups(i_anom.flatten().tolist())]

    # Group consecutive anomalies that are 2 or fewer timesteps apart
    # i_anom = i_anom.flatten().tolist()
    # groups = []
    # groups.append([i_anom[0]])
    # for i in range(1, len(i_anom)):
    #     if i_anom[i] - i_anom[i-1] <=2:
    #         groups[-1].append(i_anom[i])
    #     else:
    #         groups.append([i_anom[i]])

    # convert to timestep based on whether train_err is included. Add the end of the time window.
    if train_err is not None:
        # pruned_anom = ((i_anom - len(train_err)) * stride).tolist()
        pruned_anom = [[(g[0] - len(train_err)) * STRIDE, (g[-1] - len(train_err)) * STRIDE + WINDOW_LEN]
                       for g in groups
                       if g[-1] >= len(train_err)]  # remove anomalies detected in train data
    else:
        # pruned_anom = (i_anom * stride).tolist()
        pruned_anom = [[g[0] * STRIDE, g[-1] * STRIDE + WINDOW_LEN] for g in groups]

    # Add end of time window to each list element so each element is [start,end]
    # pruned_anom = [[i[0], i[0] + window_len] for i in pruned_anom]

    if verbose:
        print(f'Epsilon: {epsilon}. Anomaly detected at {pruned_anom}')
    return pruned_anom, epsilon