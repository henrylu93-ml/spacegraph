"""Functions for calculating and presenting performance metrics"""


def calc_confusion(pred_anom, true_anom):
    """
    Calculate the confusion matrix 
    (true positive, false positive, false negative, and true negative).

    pred_anom: a list of lists of predicted anomalies: [[start1,end1],[start2,end2]]
    true_anom: a list of lists of true anomalies

    A true negative is counted when there were no predicted nor true anomalies.

    Returns:
        A 4-tuple in this order: tp, fp, fn, tn
    """
    # True Positive and False Negative
    tp = 0
    fn = 0
    overlap = False
    for ta_start, ta_end in true_anom:
        for pa_start, pa_end in pred_anom:
            if ta_start <= pa_end and pa_start <= ta_end:
                overlap = True
        if overlap:
            tp += 1
            overlap = False
        else:
            fn += 1

    # False Positive
    fp = len(pred_anom)
    overlap = False
    for pa_start, pa_end in pred_anom:
        for ta_start, ta_end in true_anom:
            if ta_start <= pa_end and pa_start <= ta_end:
                overlap = True
        if overlap:
            fp -= 1
            overlap = False

    # True Negative
    if len(pred_anom) == 0 and len(true_anom) == 0:
        tn = 1
    else:
        tn = 0

    return tp, fp, fn, tn


def calc_precision(confusion):
    """
    Calculate precision metric. Expects input of [tp, fp, fn, tn]
    """
    tp = confusion[0]
    fp = confusion[1]
    return tp / (tp + fp)


def calc_recall(confusion):
    """
    Calculate recall metric. Expects input of [tp, fp, fn, tn]
    """
    tp = confusion[0]
    fn = confusion[2]
    return tp / (tp + fn)


def calc_f1(confusion):
    """
    Calculate F1 metric. Expects input of [tp, fp, fn, tn]
    """
    precision = calc_precision(confusion)
    recall = calc_recall(confusion)
    return 2 * precision * recall / (precision + recall)
