"""Functions to learn the graph structure"""

import causalnex.structure.dynotears as dynotears
import networkx as nx
import numpy as np
from spacegraph.config import MAX_ITERS, PAST, LAMBDA_W, LAMBDA_A, STRIDE, WINDOW_LEN, THRESHOLD


def learn_dynotears(
    pd_data,
    past=PAST,
    lambda_w=LAMBDA_W,
    lambda_a=LAMBDA_A,
    stride=STRIDE,
    window_len=WINDOW_LEN,
    force_dag=False,
    thresh=THRESHOLD,
    verbose=False
):
    """
    Learn structure from telemetry using dynotears.
    
    pd_data: the pandas data from which to learn a structure. Each row is a timestep (most recent is first).
        each column is the value of a different telemetry channel
    past: the number of past timesteps to model
    lambda_w: the regularization hyperparameter for W matrix
    lambda_a: the regularization hyperparamter for A matrix
    stride: number of timesteps from beginning of one time window to the beginning of the next
    window_len: number of timesteps in a time window
    force_dag: whether to apply remove small edge weights until graph is acyclic
    thresh: if provided, edges lower than this threshold will be removed

    Returns:
        adjacency matrices representing each structure learned
    """

    # If no stride of window_len is provided, learn one structure from all data
    if stride == 0 or window_len == 0:
        dyno = dynotears.from_pandas_dynamic(
            pd_data, past, lambda_w=lambda_w, lambda_a=lambda_a, max_iter=MAX_ITERS
        )
        adj = nx.to_numpy_matrix(dyno)
        if verbose:
            print(
                f'Learning from all data. Nodes: {dyno.number_of_nodes()}, edges: {dyno.number_of_edges()}'
            )
        return adj

    else:  # Split data into time windows
        all_adj = []

        # the remainder timesteps are discarded since we don't want to
        # learn a structure from an incomplete time window
        batches = np.floor(pd_data.shape[0] / stride
                          ).astype(int) - 1  # -1 to prevent index out of bounds

        for batch in range(batches):
            # Learn structure from a batch (i.e., time_window) of data
            start_row = batch * stride
            dyno = dynotears.from_pandas_dynamic(
                pd_data[start_row:start_row + window_len],
                past,
                lambda_w=lambda_w,
                lambda_a=lambda_a,
                max_iter=MAX_ITERS
            )

            if force_dag:  # Remove edges until graph is acyclic
                dyno.threshold_till_dag()

            if thresh is not None:  # Apply threshold if not none
                dyno.remove_edges_below_threshold(thresh)

            adj = nx.to_numpy_matrix(dyno)
            all_adj.append(adj)
            if verbose:
                print(
                    f"Learning from rows: {start_row}:{start_row+window_len}. Nodes: {dyno.number_of_nodes()}, edges: {dyno.number_of_edges()}"
                )

        return np.array(all_adj)
