"""Configuration parameters used in this project"""
import os

# Paths
HOME = os.path.expanduser("~")
ANOM_PATH = f"{HOME}/Sync/workspace/csa/neossat/anomalies_neossat4.csv"
DATA_PATH = os.path.join('data', 'spacegraph')
SAVED_PATH = os.path.join('data', 'saved')

SAVED_VERSION = 'v2'  # set version of saved data to use

PRUNE_PERCENT = 0.13  # percentage used in Telemanom pruning method

# dynotears configuration
MAX_ITERS = 10000  # maximum iterations in dynotears learning algorithm
THRESHOLD = None  # Threshold for both W and A matrices
PAST = 1  # autoregressive order, i.e. number of timesteps in the past to model
LAMBDA_W = 0.05  # L1 regularization for intra-slice edges
LAMBDA_A = 0.05  # L1 regularization for inter-slice edges
STRIDE = 125  # Learn structures of size WINDOW_LEN at every STRIDE timesteps
WINDOW_LEN = 250  # # Number of timesteps in time window

# Baseline version is v2. Different versions only change a couple of hyperparameters
# at a time.
if SAVED_VERSION == 'v3':
    LAMBDA_W = 0.01
    LAMBDA_A = 0.01
elif SAVED_VERSION == 'v4':
    LAMBDA_W = 0.1
    LAMBDA_A = 0.1
elif SAVED_VERSION == 'v5':
    PAST = 3
elif SAVED_VERSION == 'v6':
    WINDOW_LEN = 500
elif SAVED_VERSION == 'v7':
    STRIDE = 64
    WINDOW_LEN = 128