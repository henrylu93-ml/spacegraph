"""tools for plotting and visualization"""

import matplotlib.pyplot as plt
import numpy as np
from spacegraph.config import STRIDE


def plot_detected(pred_anom, true_anom, test_err, train_err=None, epsilon=None, title=None):
    """
    Plot MSE scores vs timesteps. Highlight true anomaly and detected anomaly regions.
    """

    plt.figure(figsize=(16, 4))
    # test_offset is the part of the plot that shows train data
    if train_err is not None:
        err = np.concatenate([train_err, test_err])
        test_offset = train_err.shape[0] * STRIDE
    else:
        err = test_err
        test_offset = 0

    for start, end in pred_anom:  # highlight predicted anomaly regions
        plt.axvspan(start + test_offset,
                    end + test_offset,
                    alpha=0.3,
                    color='blueviolet',
                    lw='0',
                    label='detected anomaly')

    for start, end in true_anom:  # highlight true anomaly regions
        width = 5 if start == end else 1
        plt.axvspan(start + test_offset,
                    end + test_offset,
                    alpha=0.3,
                    color='tab:red',
                    lw=width,
                    label='true anomaly')

    if epsilon is not None:  # indicate threshold
        plt.axhline(epsilon, 0, 1, color='goldenrod', linestyle='dashed', label='threshold')

    # plot train scores
    x_axis = [x * STRIDE for x in range(len(err))]
    plt.plot(x_axis, err, label='train scores')

    # plot test scores
    x_axis_test = [x * STRIDE + test_offset for x in range(len(test_err))]
    plt.plot(x_axis_test, test_err, color='mediumseagreen', label='test scores')

    plt.title(f'{title}')
    plt.xlabel(f'Timestep')
    plt.ylabel(f'Anomaly score')

    # Show legend, without duplicate entries
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = dict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys())