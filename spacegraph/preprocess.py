"""Pre-processing functions"""

import numpy as np
import pandas as pd


def find_duplicate_cols(data):
    """
    Find columns j that are duplicates of the next columns j+1
    Return indices of columns j
    """
    dupes = []
    for col in range(data.shape[1] - 1):
        if np.all(data[:, col] == data[:, col + 1]):
            dupes.append(col)
    return dupes


def find_empty_cols(data):
    """
    Find columns that contain all zeros.
    """
    return np.where(~data.any(axis=0))[0]


def fill_missing_data(data):
    """
    Fill all nans with zeros.
    """
    if isinstance(data, pd.DataFrame):
        data = data.fillna(0.0)
    elif isinstance(data, np.ndarray):
        data = np.nan_to_num(data)
    else:
        print("Invalid datatype to fill missing data")
    return data