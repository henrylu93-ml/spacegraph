"""Functions to load data and anomalies"""

import os
import pandas as pd
import numpy as np
from spacegraph.preprocess import find_duplicate_cols, fill_missing_data, find_empty_cols
from spacegraph.config import ANOM_PATH, DATA_PATH


def load_data(saf, pid, data_path=DATA_PATH, verbose=False):
    """
    Load train and test data from .csv files into pandas dataframes. 

    saf and pid must be provided to load the correct files.

    Returns:
        pd_train: pandas dataframe of train data
        pd_test: pandas dataframe of test data
    """

    # More paths
    data_file = f'{pid}-{saf}'
    train_npy = os.path.join(DATA_PATH, 'train', f'{data_file}.npy')
    test_npy = os.path.join(DATA_PATH, 'test', f'{data_file}.npy')
    train_csv = os.path.join(DATA_PATH, 'train', f'{data_file}.csv')
    test_csv = os.path.join(DATA_PATH, 'test', f'{data_file}.csv')

    # load numpy and pandas data
    np_train = np.load(train_npy)
    np_test = np.load(test_npy)
    pd_train = pd.read_csv(train_csv, delimiter=',')
    pd_test = pd.read_csv(test_csv, delimiter=',')

    # Drop first time column in pandas
    pd_train.drop(pd_train.columns[0], axis=1, inplace=True)
    pd_test.drop(pd_test.columns[0], axis=1, inplace=True)

    # Remove command columns
    cmd_cols = [col for col in pd_train if col.startswith('CMD_ACK_APPID')]
    cmd_col = pd_train.columns.get_loc(cmd_cols[0])  # get index of first command column
    np_train = np_train[:, 0:cmd_col]
    np_test = np_test[:, 0:cmd_col]
    pd_train = pd_train.iloc[:, :cmd_col]
    pd_test = pd_test.iloc[:, :cmd_col]

    # Remove duplicate columns
    dupes = find_duplicate_cols(np_train)
    np_train = np.delete(np_train, dupes, axis=1)
    np_test = np.delete(np_test, dupes, axis=1)
    pd_train.drop(pd_train.columns[dupes], axis=1, inplace=True)
    pd_test.drop(pd_test.columns[dupes], axis=1, inplace=True)

    # Fill in missing values with 0s
    pd_train = fill_missing_data(pd_train)
    pd_test = fill_missing_data(pd_test)
    np_train = fill_missing_data(np_train)
    np_test = fill_missing_data(np_test)

    # Remove columns with all zeros in train data
    empty_cols = find_empty_cols(np_train)
    np_train = np.delete(np_train, empty_cols, axis=1)
    np_test = np.delete(np_test, empty_cols, axis=1)
    pd_train.drop(pd_train.columns[empty_cols], axis=1, inplace=True)
    pd_test.drop(pd_test.columns[empty_cols], axis=1, inplace=True)

    if verbose:
        print(
            f"Loaded packet {pid} for SAF {saf}. Train: {pd_train.shape}, test: {pd_test.shape}. ")
        print(f'Mnemonics: {pd_train.columns}')

    return pd_train, pd_test


def load_labels(path=ANOM_PATH, verbose=False):
    """
    Load the .csv file containing anomaly information including time windows, PIDs, 
    description of each SAF. Returns a pandas dataframe.
    """
    pd_labels = pd.read_csv(ANOM_PATH, delimiter=',', comment='#')
    return pd_labels


def load_anom(saf, path=ANOM_PATH, verbose=False):
    # Load anomaly date to locate anomaly timestep
    pd_anoms = pd.read_csv(ANOM_PATH, delimiter=',', comment='#')
    anom_idx = pd_anoms.index[pd_anoms.loc[:, 'saf'] == saf][
        0]  # Find anomaly index corresponding to SAF
    pd_anom = pd_anoms.iloc[anom_idx]  # Anomaly row

    anom_window = pd.eval(pd_anom['anom_window'])
    pid = int(pd_anom['pid_0'])

    if verbose:
        print(f'Anomaly window: {anom_window}')

    return pid, anom_window


def load_learned_structures(saved_path, verbose=False):
    """
    Try to load the pre-learned graph structures if they were saved.

    Returns:
        adj_data: a nested numpy array containing the learned structures
    """
    try:
        adj_data = np.load(saved_path)
        if verbose:
            print(f'Loading adjacency matrices...')
    except FileNotFoundError as e:
        if verbose:
            print(f'Unable to load learned structures, file not found: {saved_path}')
        adj_data = False

    return adj_data