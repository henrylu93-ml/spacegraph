# Descriptoin
This repository contains graph-based anomaly detection methods on spacecraft telemetry. The project is called *spacegraph*.

## Contents
- `data/`
    - `saved/`: contains pre-trained or pre-learned saved data
    - `spacegraph/`: contains train and test telemetry data
- `spacegraph/`
    - `compare.py`: functions for comparing graph structures
    - `config.py`: configuration parameters used in the project
    - `detect.py`: functions for detecting anomalies from error scores
    - `load.py`: functions for loading data to work with
    - `metrics.py`: functions for calculating performance metrics
    - `preprocess.py`: functions for preprocessing data
    - `structurelearn.py`: functions for learning graph structures
    - `visualize.py`: plotting and visualization functions
- `anomrank_results.ipynb`: notebook to run experiments and display results using AnomRank graph comparison
- `mse_results.ipynb`: notebook to run experiments and display results using MSE graph comparison

# Usage
See pyproject.toml for a list of the required dependencies.
Note that some dependencies cannot be installed through poetry.
- Install packages through poetry: `poetry install` (run first)
- If pytorch is required, run: `poetry run pip install torch==1.8.0+cu101 torchvision==0.9.0+cu101 -f https://download.pytorch.org/whl/torch_stable.html`

Paths for data and labelled anomalies must be configured in `config.py`.

To run this project, open a `results.ipynb` and run all cells. 

# Saved Data and Results
Saved .npy files containing learned structures are located in data/saved/{version}/. This data is not shared publically.

**v1**:
This version contains train and test adjacency matrices generated with the following settings:
- past: 1, lambda_w: 0.05, lambda_a: 0.05, no threshold

**v2**:
This version contains adjacency matrices with the following settings:
- past: 1, lambda_w: 0.05, lambda_a: 0.05, force_dag
- data rescaling has been fixed in train/test data. constant values in train data are scaled to 1
- mse:
    - precision: 0.633
    - recall: 0.514
    - F1: 0.567
- anomrank:
    - precision: 0.559
    - recall: 0.514
    - F1: 0.535

**v3**:
- past: 1, lambda_w: 0.01, lambda_a: 0.01, force_dag
- mse:
    - precision: 0.633
    - recall: 0.514
    - F1: 0.567
- anomrank:
    - precision: 0.553
    - recall: 0.568
    - F1: 0.560

**v4**:
- past: 1, lambda_w: 0.1, lambda_a: 0.1, force_dag
- mse:
    - precision: 0.559
    - recall: 0.514
    - F1: 0.535
- anomrank:
    - precision: 0.611
    - recall: 0.595
    - F1: 0.603

**v5**:
- past: 3, lambda_w: 0.05, lambda_a: 0.05, force_dag
- mse:
    - precision: 0.586
    - recall: 0.459
    - F1: 0.515
- anomrank:
    - precision: 0.586
    - recall: 0.459
    - F1: 0.515

**v6**:
- past: 1, lambda_w: 0.05, lambda_a: 0.05, stride: 125, window_len: 500, force_dag
- mse:
    - precision: 0.643
    - recall: 0.486
    - F1: 0.554
- anomrank:
    - precision: 0.545
    - recall: 0.486
    - F1: 0.514

**v7**:
- past: 1, lambda_w: 0.05, lambda_a: 0.05, stride: 64 window_len: 128 force_dag
- mse:
    - precision: 0.486
    - recall: 0.486
    - F1: 0.486
- anomrank:
    - precision: 0.447
    - recall: 0.459
    - F1: 0.453

# Troubleshooting
Causalnex dynotears `from_panda_dynamic()` function does not recognize np.int64 indices as "int" type and will raise TypeError "Index must be integers"
- Issue: https://github.com/quantumblacklabs/causalnex/issues/86
- Workaround: use pandas v1.2.0 and numpy v1.19.5 

# References
- [DYNOTEARS algorithm from QuantumBlack](https://github.com/quantumblacklabs/causalnex)
- [Dynamic thresholding and pruning by Hundman et al., 2018](https://github.com/khundman/telemanom/tree/master/telemanom)
- [AnomRank algorithm adapted from Yoon et al., 2020](https://github.com/minjiyoon/KDD19-AnomRank)